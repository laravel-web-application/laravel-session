# Laravel Session
### Things to do list:
1. Clone this repository: `git clone https://gitlab.com/laravel-web-application/laravel-session.git`
2. Go inside the folder: `cd laravel-session`
3. Run `composer install`
4. Run `php artisan serve`

### Screen shot

Tampilkan Session

![Tampilkan Session](img/tampil.png "Tampilkan Session")

Buat Session

![Buat Session](img/buat.png "Buat Session")

Tampilkan Session

![Tampilkan Session](img/tampil2.png "Tampilkan Session")

Hapus Session

![Hapus Session](img/hapus.png "Hapus Session")
 
Tampilkan Pesan

![Tampilkan Pesan](img/pesan.png "Tampilkan Pesan")

Tampilkan Pesan Sukses

![Tampilkan Pesan Sukses](img/sukses.png "Tampilkan Pesan Sukses")

Tampilkan Pesan Gagal

![Tampilkan Pesan Gagal](img/gagal.png "Tampilkan Pesan Gagal")

Tampilkan Pesan Info

![Tampilkan Pesan Info](img/info.png "Tampilkan Pesan Info")
